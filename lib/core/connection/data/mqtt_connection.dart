import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:remote_controlled_car/core/connection/domain/connection.dart';
import 'package:typed_data/typed_buffers.dart';

const String brokerUsername = 'STAGE_OCTO';
const String brokerPassword = 'remotecontrolledcar';

const String CAMERA_STREAM_TOPIC = '/camera';
const String CONTROL_TOPIC = '/control';

class MqttConnection extends Connection {
  final String brokerAddress;
  final int portNumber;
  MqttServerClient _mqttClient;

  final StreamController<Uint8List> _mqttStream = StreamController<Uint8List>();
  final StreamController<Uint8List> _mqttSink = StreamController<Uint8List>();

  MqttConnection({@required this.brokerAddress, this.portNumber = 1883})
      : assert(brokerAddress != null) {
    _mqttClient = MqttServerClient.withPort(
        brokerAddress, UniqueKey().toString(), portNumber);
    _mqttClient.logging(on: true);
    _mqttClient.connectionMessage = MqttConnectMessage()
      ..authenticateAs(brokerUsername, brokerPassword)
      ..startClean();
    _mqttClient.connect().then((_) {
      print("CONNECTED");
      _mqttClient.subscribe(CAMERA_STREAM_TOPIC, MqttQos.atMostOnce);
      _mqttClient.updates.listen((List<MqttReceivedMessage<MqttMessage>> c) {
        if (c[0].topic == CAMERA_STREAM_TOPIC)
          _mqttStream.add((c[0].payload as MqttPublishMessage)
              .payload
              .message
              .buffer
              .asUint8List());
      });
      _mqttSink.stream.listen((event) {
        _mqttClient.publishMessage(
            CONTROL_TOPIC, MqttQos.atMostOnce, Uint8Buffer()..addAll(event));
      });
    }).catchError((e) {
      print('Exception $e');
      _mqttClient.disconnect();
    });
  }

  @override
  Sink get sink => _mqttSink.sink;

  @override
  Stream get stream => _mqttStream.stream;

  void dispose() {
    _mqttStream.close();
    _mqttSink.close();
    _mqttClient.disconnect();
  }
}
