abstract class Connection {
  Stream<dynamic> get stream;
  Sink<dynamic> get sink;
}
