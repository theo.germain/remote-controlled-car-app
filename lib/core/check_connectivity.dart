import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class CheckConnectivity {
  final Connectivity connectivity;
  final StreamController _connectivityStatusStream = StreamController<bool>();

  CheckConnectivity({@required this.connectivity})
      : assert(connectivity != null) {
    connectivity.onConnectivityChanged.listen((ConnectivityResult result) =>
        _connectivityStatusStream.sink
            .add(result == ConnectivityResult.none ? false : true));
  }

  Stream<bool> get connectivityStatusStream =>
      _connectivityStatusStream.stream.asBroadcastStream();

  void dispose() => _connectivityStatusStream.close();
}
