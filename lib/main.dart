import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/car_control/presentation/pages/car_control_view.dart';
import 'package:remote_controlled_car/main_page.dart';
import 'package:remote_controlled_car/service_locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  setup();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainPage(carControlView: locator<CarControlView>()),
      debugShowCheckedModeBanner: false,
    );
  }
}
