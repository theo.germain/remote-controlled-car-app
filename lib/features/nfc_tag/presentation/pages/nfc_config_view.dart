import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:remote_controlled_car/features/nfc_tag/presentation/view_models/nfc_tag_view_model.dart';
import 'package:remote_controlled_car/features/nfc_tag/presentation/widgets/drag_target_widget.dart';

class NfcConfigView extends StatefulWidget {
  final NfcTagViewModel nfcTagViewModel;

  NfcConfigView({@required this.nfcTagViewModel})
      : assert(nfcTagViewModel != null);

  @override
  _NfcConfigViewState createState() => _NfcConfigViewState();
}

class _NfcConfigViewState extends State<NfcConfigView> {
  List<Widget> commandList;
  int timeValue;
  double speedValue;

  @override
  void initState() {
    commandList = [DragTargetWidget()];
    timeValue = 1000;
    speedValue = 70;
    super.initState();
  }

  void removeCommandFromList(String id) =>
      setState(() => commandList.removeWhere((element) {
            return (element.runtimeType == NfcCommandWidget) &&
                ((element as NfcCommandWidget).id == id);
          }));

  void addCommandToList(NfcCommandWidget command) {
    command.commandTime = timeValue;
    if (command.command != Command.stop) command.speed = speedValue;
    setState(
      () => commandList.insert(commandList.length - 1, command),
    );
  }

  String createStringDataCommandsFromList() {
    String val = '';
    for (int i = 0; i < commandList.length - 1; i++) {
      NfcCommandWidget command = commandList[i] as NfcCommandWidget;
      val +=
          '${command.direction};${command.speed / 100.0};${command.commandTime}';
      if (i != commandList.length - 2) val += ',';
    }
    return val;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: commandList.map((e) {
                    Widget w = Padding(padding: EdgeInsets.all(5), child: e);

                    if (e.runtimeType == NfcCommandWidget) {
                      final NfcCommandWidget command = e as NfcCommandWidget;
                      String _id = command.id;
                      w = Dismissible(
                        key: UniqueKey(),
                        direction: DismissDirection.up,
                        onDismissed: (_) => removeCommandFromList(_id),
                        background: const Icon(Icons.delete_forever,
                            color: Colors.red, size: 35),
                        child: Column(children: [
                          w,
                          Text(
                            '${command.commandTime} ms - ${command.speed}%',
                            style: const TextStyle(
                                color: Colors.grey, fontSize: 11),
                          )
                        ]),
                      );
                    }
                    return w;
                  }).toList()),
            ),
            const SizedBox(height: 20),
            Wrap(
              spacing: 10,
              runSpacing: 10,
              children: [
                NfcCommandWidget(command: Command.up),
                NfcCommandWidget(command: Command.down),
                NfcCommandWidget(command: Command.left),
                NfcCommandWidget(command: Command.right),
                NfcCommandWidget(command: Command.stop),
              ]
                  .map(
                    (NfcCommandWidget nfcCommand) => Draggable(
                      child: nfcCommand,
                      feedback: Opacity(
                        opacity: 0.5,
                        child: nfcCommand,
                      ),
                      onDragCompleted: () => addCommandToList(nfcCommand),
                    ),
                  )
                  .toList(),
            ),
            Padding(
              padding: EdgeInsets.only(top: 25, bottom: 10),
              child: const Text(
                'Command Time (ms)',
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ),
            NumberPicker(
              axis: Axis.horizontal,
              minValue: 0,
              maxValue: 60000,
              step: 10,
              value: timeValue,
              itemHeight: 50,
              textStyle: TextStyle(fontSize: 15),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(12)),
              onChanged: (int val) => setState(() => timeValue = val),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                    onPressed: () => setState(() => timeValue -= 1000),
                    icon: const Icon(Icons.remove)),
                Text('current time : $timeValue ms'),
                IconButton(
                    onPressed: () => setState(() => timeValue += 1000),
                    icon: const Icon(Icons.add))
              ],
            ),
            const SizedBox(height: 20),
            Text(
              'Car Speed ($speedValue%)',
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            Slider(
              value: speedValue,
              min: 0,
              max: 100,
              divisions: 10,
              label: '$speedValue%',
              onChanged: (double value) => setState(() => speedValue = value),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: ElevatedButton(
                onPressed: commandList.length > 1
                    ? () => widget.nfcTagViewModel
                        .writeOnTag(context, createStringDataCommandsFromList())
                    : null,
                child: const Text('Write tag'),
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

enum Command { stop, up, down, left, right }

// ignore: must_be_immutable
class NfcCommandWidget extends StatelessWidget {
  final Command command;
  final String id = UniqueKey().toString();
  int commandTime;
  Image _img;
  int direction;
  double speed;

  NfcCommandWidget({@required this.command}) : assert(command != null) {
    switch (command) {
      case Command.stop:
        _img = Image.asset('assets/NFC_icons/stop.png');
        direction = 0;
        speed = 0;
        break;
      case Command.up:
        _img = Image.asset('assets/NFC_icons/up.png');
        direction = 180;
        speed = 0.8;
        break;
      case Command.down:
        _img = Image.asset('assets/NFC_icons/down.png');
        direction = 0;
        speed = 0.8;
        break;
      case Command.left:
        _img = Image.asset('assets/NFC_icons/left.png');
        direction = 90;
        speed = 0.8;
        break;
      case Command.right:
        _img = Image.asset('assets/NFC_icons/right.png');
        direction = -90;
        speed = 0.8;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12),
      child: Container(
        height: 90,
        width: 90,
        child: _img,
      ),
    );
  }
}
