import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

class DragTargetWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DragTarget(
      builder: (BuildContext context, _, __) => DottedBorder(
        child: Container(
          constraints: const BoxConstraints(maxHeight: 90, maxWidth: 90),
          height: 100,
          width: 100,
          child: Icon(
            Icons.add,
            color: Colors.grey,
            size: 35,
          ),
        ),
        color: Colors.grey,
        strokeWidth: 4,
        dashPattern: [10, 12],
        borderType: BorderType.RRect,
        strokeCap: StrokeCap.round,
        radius: const Radius.circular(12),
      ),
    );
  }
}
