import 'package:flutter/material.dart';

enum Command { stop, up, down, left, right }

// ignore: must_be_immutable
class NfcCommandWidget extends StatelessWidget {
  final Command command;
  final String id = UniqueKey().toString();
  int commandTime;
  Image _img;
  int direction;
  double speed;

  NfcCommandWidget({@required this.command}) : assert(command != null) {
    switch (command) {
      case Command.stop:
        _img = Image.asset('assets/NFC_icons/stop.png');
        direction = 0;
        speed = 0;
        break;
      case Command.up:
        _img = Image.asset('assets/NFC_icons/up.png');
        direction = 180;
        speed = 0.8;
        break;
      case Command.down:
        _img = Image.asset('assets/NFC_icons/down.png');
        direction = 0;
        speed = 0.8;
        break;
      case Command.left:
        _img = Image.asset('assets/NFC_icons/left.png');
        direction = 90;
        speed = 0.8;
        break;
      case Command.right:
        _img = Image.asset('assets/NFC_icons/right.png');
        direction = -90;
        speed = 0.8;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12),
      child: Container(
        height: 90,
        width: 90,
        child: _img,
      ),
    );
  }
}
