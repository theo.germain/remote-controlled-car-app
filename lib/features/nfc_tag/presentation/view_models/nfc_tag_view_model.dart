import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:nfc_manager/platform_tags.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';
import 'package:remote_controlled_car/features/car_control/domain/usecases/send_car_direction_usecase.dart';

enum NfcWriteStatus { none, sucess, failure }

class NfcTagViewModel with ChangeNotifier {
  final SendCarDirectionUsecase sendCarDirectionUsecase;

  final StreamController<CarControl> _controlStream =
      StreamController<CarControl>();

  NfcTagViewModel({@required this.sendCarDirectionUsecase})
      : assert(sendCarDirectionUsecase != null) {
    sendCarDirectionUsecase(_controlStream.stream);
  }

  Future<void> initRead() async {
    if (await NfcManager.instance.isAvailable()) {
      NfcManager.instance.stopSession();
      NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
        print("New tag found");
        if (tag.data.keys.contains('ndef')) {
          print('NDEF');

          Ndef ndefTag = Ndef.from(tag);
          final NdefMessage msg = await ndefTag.read();
          if (msg.records.length >= 2) {
            List<String> commands = String.fromCharCodes(msg.records[1].payload
                    .getRange(3, msg.records[1].payload.length))
                .split(',');
            print(commands);
            for (var command in commands) {
              List<String> values = command.split(';');
              _controlStream.sink.add(CarControl(
                  direction: double.parse(values[0]),
                  speed: double.parse(values[1])));
              await Future.delayed(
                  Duration(milliseconds: int.parse(values[2])));
            }
            _controlStream.sink.add(CarControl(direction: 0, speed: 0));
          }
        } else {
          print("NOT NDEF");
        }
      });
    }
  }

  Future<void> writeOnTag(BuildContext context, String data) async {
    NfcWriteStatus status = NfcWriteStatus.none;

    final NdefMessage msg = NdefMessage([
      NdefRecord.createExternal('android.com', 'pkg',
          Uint8List.fromList('com.example.remote_controlled_car'.codeUnits)),
      NdefRecord.createText(data),
    ]);

    if (await NfcManager.instance.isAvailable()) {
      NfcManager.instance.stopSession();
      NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
        print("New tag found");
        if (tag.data.keys.contains('ndef')) {
          Ndef ndefTagToWrite = Ndef.from(tag);
          if (ndefTagToWrite.isWritable) {
            try {
              await ndefTagToWrite.write(msg);
              NfcManager.instance.stopSession();
              status = NfcWriteStatus.sucess;
            } catch (e) {
              status = NfcWriteStatus.failure;
              print(e);
            }
          }
        } else if (tag.data.keys.contains('ndefformatable')) {
          NdefFormatable ndefFormatableTagToWrite = NdefFormatable.from(tag);
          try {
            await ndefFormatableTagToWrite.format(msg);
            NfcManager.instance.stopSession();
            status = NfcWriteStatus.sucess;
          } catch (e) {
            status = NfcWriteStatus.failure;
            print(e);
          }
        }
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            duration: const Duration(seconds: 1),
            content: Text(
              status == NfcWriteStatus.sucess ? 'SUCCESS' : 'FAILURE',
              textAlign: TextAlign.center,
            ),
            elevation: 3,
            backgroundColor:
                status == NfcWriteStatus.sucess ? Colors.green : Colors.red,
            margin: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 4,
                vertical: 50),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12))),
            behavior: SnackBarBehavior.floating,
          ));
      });
    }
    return;
  }

  @override
  void dispose() {
    _controlStream.close();
    NfcManager.instance.stopSession();
    super.dispose();
  }
}
