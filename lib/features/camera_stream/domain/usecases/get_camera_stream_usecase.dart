import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/repositories/camera_stream_repository.dart';

class GetCameraStreamUsecase {
  final CameraStreamRepository repository;

  GetCameraStreamUsecase({@required this.repository})
      : assert(repository != null);

  Stream<Uint8List> call() => repository.getCameraStream();
}
