import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/usecases/get_camera_stream_usecase.dart';

GetIt locator = GetIt.instance;

void cameraStreamDomainSetup() {
  // usecases
  locator.registerLazySingleton(
      () => GetCameraStreamUsecase(repository: locator()));
}
