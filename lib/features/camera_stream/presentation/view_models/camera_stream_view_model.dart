import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/usecases/get_camera_stream_usecase.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/widgets/battery_level_widget.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/widgets/no_image_widget.dart';
import 'package:remote_controlled_car/features/image_classifier/view_models/image_classifier_view_model.dart';

class CameraStreamViewModel with ChangeNotifier {
  final GetCameraStreamUsecase getCameraStreamUsecase;
  final StreamController<Widget> _cameraStream =
      StreamController<Widget>.broadcast();
  final ImageClassifierViewModel imageClassifierViewModel;
  Timer checkCameraStream;
  DateTime lastImageReceived;
  final _batteryLevel = BatteryLevelWidget();

  //int fpsCount = 0;

  CameraStreamViewModel(
      {@required this.getCameraStreamUsecase,
      @required this.imageClassifierViewModel})
      : assert(getCameraStreamUsecase != null),
        assert(imageClassifierViewModel != null) {
    /*Timer.periodic(const Duration(seconds: 1), (_) {
      print('$fpsCount FPS');
      fpsCount = 0;
    });*/

    getCameraStreamUsecase().listen((Uint8List imgBuffer) async {
      //fpsCount++;

      Image image = Image.memory(imgBuffer,
          gaplessPlayback: true, width: double.infinity, fit: BoxFit.fitWidth);

      lastImageReceived = DateTime.now();

      _cameraStream.sink.add(
        Stack(
          alignment: AlignmentDirectional.topEnd,
          children: [
            image,
            imageClassifierViewModel.state
                ? imageClassifierViewModel
                    .getPredictionsForBinaryImage(imgBuffer)
                : Container(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: _batteryLevel,
            )
          ],
        ),
      );
    });
    _cameraStream.sink.add(NoImageWidget());
    checkCameraStream = Timer.periodic(Duration(seconds: 5), (_) {
      if (lastImageReceived != null &&
          lastImageReceived
              .isBefore(DateTime.now().subtract(Duration(seconds: 5))))
        _cameraStream.sink.add(NoImageWidget());
    });
  }

  Stream<Widget> get cameraStream => _cameraStream.stream;

  @override
  void dispose() {
    _cameraStream.close();
    checkCameraStream.cancel();
    super.dispose();
  }
}
