import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/features/camera_stream/data/repositories/camera_stream_datasource.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/repositories/camera_stream_repository.dart';

class CameraStreamRepositoryImpl extends CameraStreamRepository {
  final CameraStreamDatasource datasource;

  CameraStreamRepositoryImpl({@required this.datasource})
      : assert(datasource != null);

  @override
  Stream<Uint8List> getCameraStream() => datasource.getCameraStream();
}
