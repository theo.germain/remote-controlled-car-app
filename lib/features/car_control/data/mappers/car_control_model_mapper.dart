import 'package:remote_controlled_car/features/car_control/data/models/car_control_model.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';

class CarControlModelMapper {
  static CarControlModel entityToModel(CarControl carControl) =>
      CarControlModel(direction: carControl.direction, speed: carControl.speed);
}
