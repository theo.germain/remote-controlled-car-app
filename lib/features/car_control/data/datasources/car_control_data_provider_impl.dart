import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/core/connection/domain/connection.dart';
import 'package:remote_controlled_car/features/car_control/data/models/car_control_model.dart';
import 'package:remote_controlled_car/features/car_control/data/repositories/car_control_data_provider.dart';

class CarControlDataProviderImpl extends CarControlDataProvider {
  final Connection connection;

  CarControlDataProviderImpl({@required this.connection})
      : assert(connection != null);

  @override
  void sendCarControlData(Stream<CarControlModel> data) =>
      data.listen((dataControl) => connection.sink.add(
          Uint8List.fromList(json.encode(dataControl.toJson()).codeUnits)));
}
