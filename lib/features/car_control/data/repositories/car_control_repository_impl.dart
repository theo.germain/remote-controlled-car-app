import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/features/car_control/data/mappers/car_control_model_mapper.dart';
import 'package:remote_controlled_car/features/car_control/data/repositories/car_control_data_provider.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';
import 'package:remote_controlled_car/features/car_control/domain/repositories/car_control_repository.dart';

class CarControlRepositoryImpl implements CarControlRepository {
  final CarControlDataProvider dataProvider;

  CarControlRepositoryImpl({@required this.dataProvider})
      : assert(dataProvider != null);

  @override
  void sendCarDirection(Stream<CarControl> stream) {
    dataProvider.sendCarControlData(stream.map(
        (CarControl control) => CarControlModelMapper.entityToModel(control)));
  }
}
