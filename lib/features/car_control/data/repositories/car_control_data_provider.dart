import 'package:remote_controlled_car/features/car_control/data/models/car_control_model.dart';

abstract class CarControlDataProvider {
  void sendCarControlData(Stream<CarControlModel> data);
}
