import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class CarControlModel extends Equatable {
  final double speed;
  final double direction;

  CarControlModel({@required this.speed, @required this.direction})
      : assert(speed != null),
        assert(direction != null);

  Map<String, dynamic> toJson() => {'speed': speed, 'direction': direction};

  @override
  List<Object> get props => [speed, direction];
}
