import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/car_control/data/datasources/car_control_data_provider_impl.dart';
import 'package:remote_controlled_car/features/car_control/data/repositories/car_control_data_provider.dart';
import 'package:remote_controlled_car/features/car_control/data/repositories/car_control_repository_impl.dart';
import 'package:remote_controlled_car/features/car_control/domain/repositories/car_control_repository.dart';

GetIt locator = GetIt.instance;

void carControlDataSetup() {
  // repositories
  locator.registerLazySingleton<CarControlRepository>(
      () => CarControlRepositoryImpl(dataProvider: locator()));

  // datasources
  locator.registerLazySingleton<CarControlDataProvider>(
      () => CarControlDataProviderImpl(connection: locator()));
}
