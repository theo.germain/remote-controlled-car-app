import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';
import 'package:remote_controlled_car/features/car_control/domain/repositories/car_control_repository.dart';

class SendCarDirectionUsecase {
  final CarControlRepository repository;

  SendCarDirectionUsecase({@required this.repository})
      : assert(repository != null);

  void call(Stream<CarControl> stream) => repository.sendCarDirection(stream);
}
