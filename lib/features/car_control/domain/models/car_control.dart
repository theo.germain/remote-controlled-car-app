import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class CarControl extends Equatable {
  final double speed;
  final double direction;

  CarControl({@required this.direction, @required this.speed})
      : assert(direction != null),
        assert(speed != null);

  @override
  List<Object> get props => [speed, direction];
}
