import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remote_controlled_car/core/check_connectivity.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';
import 'package:remote_controlled_car/features/car_control/domain/usecases/send_car_direction_usecase.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

class VoiceControlViewModel with ChangeNotifier {
  final StreamController<CarControl> _controlStream =
      StreamController<CarControl>();
  final SpeechToText speech;
  final SendCarDirectionUsecase sendCarDirectionUsecase;
  final CheckConnectivity checkConnectivity;
  String _currentLocaleId;
  bool listeningState = false;
  bool isDeviceConnected = false;

  VoiceControlViewModel(
      {@required this.speech,
      @required this.sendCarDirectionUsecase,
      @required this.checkConnectivity})
      : assert(speech != null),
        assert(sendCarDirectionUsecase != null),
        assert(checkConnectivity != null) {
    initSpeech();
    sendCarDirectionUsecase(_controlStream.stream);
    checkConnectivity.connectivityStatusStream.listen((event) {
      isDeviceConnected = event;
      notifyListeners();
    });
  }

  void initSpeech() {
    speech
        .initialize(
            onError: errorListener,
            //onStatus: statusListener,
            debugLogging: true)
        .then((hasSpeech) => speech
            .systemLocale()
            .then((systemLocale) => _currentLocaleId = systemLocale.localeId));
  }

  void startListening() {
    speech.listen(
        onResult: resultListener,
        partialResults: false,
        localeId: _currentLocaleId,
        cancelOnError: true,
        listenMode: ListenMode.confirmation);
    listeningState = true;
  }

  void resultListener(SpeechRecognitionResult result) async {
    print('RESULT');
    stopListening();
    String words = result.recognizedWords;
    if (words.contains('avant') || words.contains('avance'))
      _controlStream.sink.add(CarControl(direction: 180, speed: 0.75));
    else if (words.contains('stop'))
      _controlStream.sink.add(CarControl(direction: 0, speed: 0));
    else if (words.contains('arrière') || words.contains('recule'))
      _controlStream.sink.add(CarControl(direction: 0, speed: 0.75));
    else if (words.contains('droite'))
      _controlStream.sink.add(CarControl(direction: -90, speed: 0.75));
    else if (words.contains('gauche'))
      _controlStream.sink.add(CarControl(direction: 90, speed: 0.75));

    await Future.delayed(Duration(milliseconds: 80));
    startListening();
  }

  void errorListener(SpeechRecognitionError error) async {
    stopListening();
    print('ERROR');
    print(error.errorMsg);
    if (error.errorMsg != 'error_network') {
      await Future.delayed(Duration(milliseconds: 80));
      startListening();
    } else
      notifyListeners();
  }

  /*void statusListener(String status) =>
      listeningState = status == 'listening' ? true : false;*/

  void stopListening() {
    speech.stop();
    listeningState = false;
  }

  void cancelListening() {
    speech.cancel();
    listeningState = false;
  }

  void _toggleListeningState() {
    listeningState ? cancelListening() : startListening();
    notifyListeners();
  }

  Function get toggleListeningState =>
      isDeviceConnected ? _toggleListeningState : null;

  IconData get icon =>
      listeningState ? Icons.mic_off_outlined : Icons.mic_none_outlined;

  Color get iconColor => isDeviceConnected ? Colors.greenAccent : Colors.grey;

  @override
  void dispose() {
    cancelListening();
    _controlStream.close();
    super.dispose();
  }
}
