import 'package:control_pad/control_pad.dart';
import 'package:flutter/cupertino.dart';

class JoystickWidget extends StatelessWidget {
  final Sink<List<double>> dataOutput;

  JoystickWidget({Key key, @required this.dataOutput})
      : assert(dataOutput != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return JoystickView(onDirectionChanged: (double degrees, double distance) {
      //print('Degrés : $degrees, Distance : $distance');
      dataOutput.add([degrees, distance]);
    });
  }
}
