import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remote_controlled_car/features/car_control/presentation/view_models/voice_control_view_model.dart';

class VoiceControlButtonWidget extends StatelessWidget {
  final VoiceControlViewModel voiceControlViewModel;

  VoiceControlButtonWidget({@required this.voiceControlViewModel})
      : assert(voiceControlViewModel != null);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: voiceControlViewModel,
      builder: (BuildContext context, _) => TextButton(
        onPressed:
            Provider.of<VoiceControlViewModel>(context).toggleListeningState,
        child: Icon(
          Provider.of<VoiceControlViewModel>(context).icon,
          size: 30,
          color: Provider.of<VoiceControlViewModel>(context).iconColor,
        ),
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(13),
            shape: CircleBorder(
                side: BorderSide(
                    color: Provider.of<VoiceControlViewModel>(context)
                        .iconColor))),
      ),
    );
  }
}
