abstract class FlashLedRepository {
  void setCarLedState(bool isLedOn);
}