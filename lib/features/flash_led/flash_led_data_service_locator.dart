import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/flash_led/data/datasources/flash_led_datasource_impl.dart';
import 'package:remote_controlled_car/features/flash_led/data/repositories/flash_led_datasource.dart';
import 'package:remote_controlled_car/features/flash_led/data/repositories/flash_led_repository_impl.dart';
import 'package:remote_controlled_car/features/flash_led/domain/repositories/flash_led_repository.dart';

GetIt locator = GetIt.instance;

void flashLedDataSetup() {
  // repositories
  locator.registerLazySingleton<FlashLedRepository>(
      () => FlashLedRepositoryImpl(datasource: locator()));

  // datasources
  locator.registerLazySingleton<FlashLedDatasource>(
      () => FlashLedDatasourceImpl(connection: locator()));
}
