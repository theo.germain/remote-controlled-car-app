abstract class FlashLedDatasource {
  void sendFlashLedStateUpdate(bool isLedOn);
}
