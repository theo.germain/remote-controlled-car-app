import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/core/connection/domain/connection.dart';
import 'package:remote_controlled_car/features/flash_led/data/repositories/flash_led_datasource.dart';

class FlashLedDatasourceImpl implements FlashLedDatasource {
  Connection connection;

  FlashLedDatasourceImpl({@required this.connection})
      : assert(connection != null);

  @override
  void sendFlashLedStateUpdate(bool isLedOn) {
    Map<String, dynamic> payload = {'isLedOn': isLedOn};
    String jsonMessage = json.encode(payload);
    connection.sink.add(Uint8List.fromList(jsonMessage.codeUnits));
  }
}
