import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/flash_led/domain/usecases/set_car_led_state_usecase.dart';

GetIt locator = GetIt.instance;

void flashLedDomainSetup() {
  // usecases
  locator.registerLazySingleton(
      () => SetCarLedStateUsecase(repository: locator()));
}
