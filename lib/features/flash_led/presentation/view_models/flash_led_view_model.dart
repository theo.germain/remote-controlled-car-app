import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/flash_led/domain/usecases/set_car_led_state_usecase.dart';

class FlashLedViewModel with ChangeNotifier {
  final SetCarLedStateUsecase usecase;
  bool isFlashOn = false;

  FlashLedViewModel({@required this.usecase}) : assert(usecase != null);

  IconData get icon => isFlashOn ? Icons.flash_off : Icons.flash_on;

  void toggleFlashState() {
    isFlashOn = !isFlashOn;
    usecase(isFlashOn);
    notifyListeners();
  }
}
