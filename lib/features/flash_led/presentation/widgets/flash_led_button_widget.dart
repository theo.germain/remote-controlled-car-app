import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remote_controlled_car/features/flash_led/presentation/view_models/flash_led_view_model.dart';

class FlashLedButtonWidget extends StatelessWidget {
  final FlashLedViewModel viewModel;

  FlashLedButtonWidget({@required this.viewModel}) : assert(viewModel != null);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: viewModel,
      builder: (BuildContext context, _) => TextButton(
        onPressed: Provider.of<FlashLedViewModel>(context).toggleFlashState,
        child: ShaderMask(
          blendMode: BlendMode.srcIn,
          shaderCallback: (Rect bound) => LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: <Color>[Colors.red, Colors.yellow]).createShader(bound),
          child: Icon(
            Provider.of<FlashLedViewModel>(context).icon,
            size: 40,
          ),
        ),
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(13),
            shape: CircleBorder(side: BorderSide(color: Colors.amber))),
      ),
    );
  }
}
