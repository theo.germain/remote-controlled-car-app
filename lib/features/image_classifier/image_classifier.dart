import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:image/image.dart' as img;
import 'package:remote_controlled_car/features/image_classifier/classifier.dart';
import 'package:remote_controlled_car/features/image_classifier/models/recognition_model.dart';
import 'package:remote_controlled_car/features/image_classifier/widgets/box_widget.dart';

class ImageClassifier {
  final Classifier classifier;
  static double screenWidth = MediaQueryData.fromWindow(window).size.width;

  ImageClassifier({@required this.classifier}) : assert(classifier != null);

  Widget runOnBinaryImage({Uint8List imageData}) {
    final List<RecognitionModel> results =
        classifier.predict(img.decodeJpg(imageData));
    return _boundingBoxes(results);
  }

  /// Returns Stack of bounding boxes
  Widget _boundingBoxes(List<RecognitionModel> results) {
    if (results == null) {
      return Container();
    }
    return Stack(
      children: results
          .map((result) => BoxWidget(
                result: result,
              ))
          .toList(),
    );
  }
}
