import 'dart:async';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/repositories/camera_stream_repository.dart';
import 'package:remote_controlled_car/features/camera_stream/domain/usecases/get_camera_stream_usecase.dart';

class MockCameraStreamRepository extends Mock
    implements CameraStreamRepository {}

void main() {
  MockCameraStreamRepository repository;
  GetCameraStreamUsecase usecase;

  setUp(() {
    repository = MockCameraStreamRepository();
    usecase = GetCameraStreamUsecase(repository: repository);
  });

  group('call function', () {
    test('Should call the repository getCameraStream method', () {
      // When
      usecase();
      // Then
      verify(repository.getCameraStream());
    });

    test('Should return the stream returned by the repository', () {
      // Given
      final Stream<Uint8List> stream = StreamController<Uint8List>().stream;
      when(repository.getCameraStream()).thenAnswer((_) => stream);
      // When
      final result = usecase();
      // Then
      expect(result, stream);
    });
  });
}
