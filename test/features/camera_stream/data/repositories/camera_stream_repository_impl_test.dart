import 'dart:async';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/features/camera_stream/data/repositories/camera_stream_datasource.dart';
import 'package:remote_controlled_car/features/camera_stream/data/repositories/camera_stream_repository_impl.dart';

class MockCameraStreamDatasource extends Mock
    implements CameraStreamDatasource {}

void main() {
  MockCameraStreamDatasource datasource;
  CameraStreamRepositoryImpl repository;

  setUp(() {
    datasource = MockCameraStreamDatasource();
    repository = CameraStreamRepositoryImpl(datasource: datasource);
  });

  group('getCameraStream function', () {
    test('Should call the datasource getCameraStream method', () {
      // When
      repository.getCameraStream();
      // Then
      verify(datasource.getCameraStream());
    });

    test('Should return the stream returned by the datasource', () {
      // Given
      final Stream<Uint8List> stream = StreamController<Uint8List>().stream;
      when(datasource.getCameraStream()).thenAnswer((_) => stream);
      // When
      final result = repository.getCameraStream();
      // Then
      expect(result, stream);
    });
  });
}
