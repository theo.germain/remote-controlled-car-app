import 'package:flutter_test/flutter_test.dart';
import 'package:remote_controlled_car/features/car_control/data/mappers/car_control_model_mapper.dart';
import 'package:remote_controlled_car/features/car_control/data/models/car_control_model.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';

void main() {
  group('entityToModel', () {
    test(
        'should return an instance of CarControlModel with the same speed and directions values as the entity',
        () {
      // Given
      final CarControl carControl = CarControl(direction: 90, speed: 0.5);
      final CarControlModel expectedModel =
          CarControlModel(direction: 90, speed: 0.5);

      // When
      final result = CarControlModelMapper.entityToModel(carControl);

      // Then
      expect(result, isA<CarControlModel>());
      expect(result, expectedModel);
    });
  });
}
