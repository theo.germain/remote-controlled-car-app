import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/core/connection/data/web_socket_connection_impl.dart';
import 'package:remote_controlled_car/features/car_control/data/datasources/car_control_data_provider_impl.dart';
import 'package:remote_controlled_car/features/car_control/data/models/car_control_model.dart';

class MockWebSocketConnection extends Mock implements WebSocketConnectionImpl {}

class MockSink extends Mock implements Sink {}

void main() {
  CarControlDataProviderImpl dataProvider;
  MockWebSocketConnection mockWebSocketConnection;

  setUp(() {
    mockWebSocketConnection = MockWebSocketConnection();
    dataProvider =
        CarControlDataProviderImpl(connection: mockWebSocketConnection);
  });

  group('sendCarControlData', () {
    test('', () async {
      // Given
      final StreamController<CarControlModel> dataStream =
          StreamController<CarControlModel>();
      final CarControlModel carControlModel =
          CarControlModel(speed: 1.0, direction: 180);
      final MockSink mockWebSocketConnectionSink = MockSink();
      when(mockWebSocketConnection.sink)
          .thenReturn(mockWebSocketConnectionSink);

      // When
      dataProvider.sendCarControlData(dataStream.stream);
      dataStream.add(carControlModel);

      // Then
      await untilCalled(mockWebSocketConnection.sink);
      await untilCalled(mockWebSocketConnectionSink.add(Uint8List.fromList(
          (json.encode(carControlModel.toJson()).codeUnits))));

      dataStream.close();
    });
  });
}
