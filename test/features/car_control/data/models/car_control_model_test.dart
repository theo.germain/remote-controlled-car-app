import 'package:flutter_test/flutter_test.dart';
import 'package:remote_controlled_car/features/car_control/data/models/car_control_model.dart';

void main() {
  group('toJson', () {
    test('should return a JSON map containing the proper data', () {
      // Given
      final CarControlModel carControlModel =
          CarControlModel(speed: 0.5, direction: -90);
      final Map<String, dynamic> expectedJsonMap = {
        "speed": 0.5,
        "direction": -90
      };

      // When
      final result = carControlModel.toJson();

      // Then
      expect(result, expectedJsonMap);
    });
  });
}
